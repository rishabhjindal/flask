from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('home.html')

@app.route('/reverse/<name>')
def deg_name_reverse(name):
    output_name = ''
    for str in name:
        output_name = str + output_name
    reverse_name = output_name
    return 'Reverse of string is %s.' %reverse_name

if __name__ == '_main__':
    app.run(debug=True)
